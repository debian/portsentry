# portsentry
# Copyright (C) 2007
# This file is distributed under the same license as the portsentry package.
# Hans Fredrik Nordhaug <hans@nordhaug.priv.no>, 2007
#
msgid ""
msgstr ""
"Project-Id-Version: portsentry\n"
"Report-Msgid-Bugs-To: bruno@debian.org\n"
"POT-Creation-Date: 2006-10-29 19:45-0300\n"
"PO-Revision-Date: 2007-03-12 00:56+0100\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.ui.no>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: note
#. Description
#: ../templates:1001
msgid "PortSentry does not block anything by default."
msgstr "PortSentry blokkerer ingenting som standard."

#. Type: note
#. Description
#: ../templates:1001
msgid ""
"Please note that by default PortSentry takes no action against potential "
"attackers. It only dumps messages into /var/log/syslog. To change this edit /"
"etc/portsentry/portsentry.conf."
msgstr ""
"Legg merke til at PortSentry som standard ikke gjør noe med potensielle\n"
"angripere. Det bare skriver meldinger inn i /var/log/syslog. For å endre "
"dette,\n"
"rediger /etc/portsentry/portsentry.conf."

#. Type: note
#. Description
#: ../templates:1001
msgid ""
" You may also want to check:\n"
" /etc/default/portsentry (daemon startup options) and\n"
" /etc/portsentry/portsentry.ignore.static (hosts/interfaces to ignore)"
msgstr ""
"Du vil kanskje også sjekke:\n"
" /etc/default/portsentry (nisseoppstartsinnstillinger) og\n"
" /etc/portsentry/portsentry.ignore.static (tjenere/grensesnitt å ignorere)"

#. Type: note
#. Description
#: ../templates:1001
msgid ""
"For further details see the portsentry(8) and portsentry.conf(5) manpages."
msgstr "For flere detaljer, se mansidene portsentry(8) og portsentry.conf(5)."

#. Type: note
#. Description
#: ../templates:2001
msgid "startup.conf is obsolete - use /etc/default/portsentry instead"
msgstr "startup.conf er foreldet - bruk /etc/default/portsentry i steden for"

#. Type: note
#. Description
#: ../templates:2001
msgid ""
"/etc/portsentry/startup.conf is no longer used and /etc/default/portsentry "
"is used instead. In order to ease the transition I'll do my best to preserve "
"your settings while copying them over to the new location.  Please check /"
"etc/default/portsentry against /etc/portsentry/startup.conf and remove it "
"after the installation has finished."
msgstr ""
"/etc/portsentry/startup.conf brukes ikke lenger og istedenfor brukes\n"
"/etc/default/portsentry. For å lette overgangen, vil jeg gjøre mitt beste \n"
"ved å bevare dine innstillnger når jeg kopierer dem over til den nye file.\n"
"Sjekk /etc/default/portsentry mot /etc/portsentry/startup.conf, og fjern\n"
"startup.conf etter at installasjonen er fullført."

#. Type: note
#. Description
#: ../templates:2001
msgid "Sorry for any inconvenience."
msgstr "Beklager bryderiet."
